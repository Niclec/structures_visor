/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.control;

import java.io.File;
import java.util.Objects;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;




/**
 *
 * @author user
 */
public class Imagenes implements Comparable<Imagenes>{
    private String urlImagen;
    private double sizex, sizey;
    private int stars;
    private ImageView im;
    
    public Imagenes(){
    }
    
    public Imagenes(String urlImagen){
        this.urlImagen = urlImagen;
        this.sizex = this.sizey = 200;
        this.stars = 0;
    }
    
    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public double getSizex() {
        return sizex;
    }

    public void setSizex(double sizex) {
        this.sizex = sizex;
    }

    public double getSizey() {
        return sizey;
    }

    public void setSizey(double sizey) {
        this.sizey = sizey;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public ImageView getIm() {
        return im;
    }

    public void setIm(ImageView im) {
        this.im = im;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.sizey) ^ (Double.doubleToLongBits(this.sizey) >>> 32));
        hash = 67 * hash + this.stars;
        hash = 67 * hash + Objects.hashCode(this.im);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Imagenes other = (Imagenes) obj;
        if (!Objects.equals(this.urlImagen, other.urlImagen)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Imagenes t) {
        if(this.stars>t.getStars()){
            return 1;
        }
        else if(this.stars<t.getStars()){
            return -1;
        
        }
        return 0;
        
    }


    
    
    
    
}
