package espol.edu.ec.control;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class ControlImages {

	private List<Imagenes> imagenes = new LinkedList<>();
        private int limite;
	private static ControlImages controlImages;
        private Imagenes actual;
	
	private ControlImages() {this.limite = 0;
}
	
	public static ControlImages getSingletonInstance() {
        if (controlImages == null){
            controlImages = new ControlImages();
            
        }
        else{
        	Logger.getLogger(ControlImages.class.getName()).log(Level.SEVERE, null,new Exception());
        }
        
        return controlImages;
    }

    public Imagenes getActual() {
        return actual;
    }

    public void setActual(Imagenes actual) {
        this.actual = actual;
    }
	
	public Imagenes guardarImagenes(String path) {
		Imagenes im = new Imagenes(path);
                if(imagenes.isEmpty())
                    this.actual = im;
		imagenes.add(im);
                return im;
	}
	
	public void eliminarImagenes(Imagenes im) {
		imagenes.remove(im);
                this.limite--;
	}

	public List<Imagenes> getImagenes() {
		return imagenes;
	}
        
        public Imagenes getImagenesbyName(String Path){
            Imagenes retorno = new Imagenes();
            for(Imagenes im: this.imagenes)
                if(im.getUrlImagen().endsWith(Path))
                    return im;
            return retorno;
        }
        
	public void setImagenes(List<Imagenes> imagenes) {
		this.imagenes = imagenes;
	}
	
        public Imagenes cambiarImagenNext(){
            this.limite=this.imagenes.indexOf(this.actual);
            if(this.imagenes.isEmpty())
                return new Imagenes();
            if(limite<this.imagenes.size()-1){
                limite++;
                actual = this.imagenes.get(limite);
                return actual;
            }
               
            return this.imagenes.get(limite);                    
        }
        
        public Imagenes cambiarPreviousImagen(){
            this.limite=this.imagenes.indexOf(this.actual);
            if(this.imagenes.isEmpty())
                return new Imagenes();
            
            if(limite>=1){
                limite--;
               this.actual=this.imagenes.get(limite);
               return actual; 
            }
               
            return this.imagenes.get(limite);
        }
	
	public List<Imagenes> tomarImagenes(String directoryPath){
            File dir = new File(directoryPath);
            File[] listFiles = dir.listFiles();
            if(listFiles!=null)
                for(File file: listFiles){
                    ImageView im = new ImageView(new Image(file.toURI().toString()));
                    im.setFitHeight(400);
                    im.setFitWidth(500);
                    this.guardarImagenes(file.getAbsolutePath()).setIm(im);
                    
                }
            return this.imagenes;
        }
        
        public boolean eliminarImageFile(String directoryPath){
            File file = new File(directoryPath);
            return file.delete();
        }

}
