/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.control;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author user
 */
public class VistaImages {
    
    private final ControlImages control = ControlImages.getSingletonInstance();
    private BorderPane root;
    
    public VistaImages(BorderPane root){
        this.root = root;
        this.root.setPadding(new Insets(10,10,10,10));
        this.inicializarImages();
        
    }
    
    public static void ordenarALinked(){
        
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
        this.root.autosize();
        this.root.applyCss();
    }

    public ControlImages getControl() {
        return control;
    }
    
    public void tomarImagen(){
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("View Pictures");
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.dir"))
        );                 
        fileChooser.getExtensionFilters().addAll(
            //new FileChooser.ExtensionFilter("All Images", "*.*"),
            new FileChooser.ExtensionFilter("JPG", "*.jpg"),
            new FileChooser.ExtensionFilter("PNG", "*.png")
            );
        
        List<File> list = (List<File>) fileChooser.showOpenMultipleDialog((Stage) this.root.getScene().getWindow());
                    if (list != null) {
                        list.forEach((file) -> {
                            saveImage(file);
            });
        }
        
    }
    
    public void goBack(){
        
        int place = this.control.getImagenes().indexOf(this.control.getActual());
        System.out.println(place);
        String pathDeleted = this.control.getActual().getUrlImagen();
        
        if(place>0){
            this.root.setCenter(this.control.getImagenes().get(place-1).getIm());
            this.control.setActual(this.control.getImagenes().get(place-1));
        }else if(!this.control.getImagenes().isEmpty()&&place==0){
            this.control.setActual(this.control.getImagenes().get(place-1));
            this.root.setCenter(this.control.getImagenes().get(place+1).getIm());
        }
        this.root.setCenter(this.control.getActual().getIm());
        this.control.eliminarImagenes(this.control.getImagenes().get(place));
        this.control.eliminarImageFile(pathDeleted);
    }
        
     /***
       * Método que guarda la imagen en la aplicación
       */
    private void saveImage(File file){
        String destPath = System.getProperty("user.dir");
        destPath += "/src/espol/edu/ec/resources/";
        File file2 = new File(destPath);
            
        try {
            FileUtils.copyFileToDirectory(file, file2);           
            ImageView im = new ImageView(new Image(file.toURI().toString()));
            this.control.guardarImagenes(destPath+file.getName()).setIm(im);
            im.setFitHeight(400);
            im.setFitWidth(500);
            this.root.setCenter(im);
        } catch (IOException ex) {
            Logger.getLogger(VistaImages.class.getName()).log(Level.SEVERE, null, ex);
            
        }
                
 
    }
    
    public void changeImage(Imagenes img){
        this.root.setCenter(img.getIm());
    }
    
    public Imagenes changeImage(int next){
        Imagenes img;
        this.root.getChildren().clear();
        if(next>0){
            img =this.control.cambiarImagenNext();
            
            this.root.setCenter(img.getIm());
            return img;            
        }
        img =this.control.cambiarPreviousImagen();
        this.root.setCenter(img.getIm());
        return img;
    }
    
    private void inicializarImages(){
        String localPath = System.getProperty("user.dir")+"/src/espol/edu/ec/resources/";
        
        List<Imagenes> im = this.control.tomarImagenes(localPath);
        if(im!=null)
            this.root.setCenter(im.get(0).getIm());
    }
    
    
}

class LocatedImage extends Image {
    private final String url;

    public LocatedImage(String url) {
        super(url);
        this.url = url;
    }

    public String getURL() {
        return url;
    }
}


   
    

