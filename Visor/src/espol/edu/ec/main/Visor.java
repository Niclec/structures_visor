/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.main;

import espol.edu.ec.views.PanePrincipal;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author user
 */
public class Visor extends Application {
    
    @Override
    public void start(Stage primaryStage) {
       PanePrincipal organizer=new PanePrincipal();
        
        Scene scene = new Scene(organizer.getRoot(), 900, 600);
        scene.getStylesheets().add(getClass().getResource("mystyle.css").toExternalForm());
        primaryStage.setTitle("Visor");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("/espol/edu/ec/resources/imageFolder1.jpg"));
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
