package espol.edu.ec.views;

import espol.edu.ec.control.ControlImages;
import espol.edu.ec.control.Imagenes;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ViewTreeItem {

    private TreeItem<String> rootItem, totales,favoritos;
    private TreeView<String> tree;
    private String currentFile;

    public ViewTreeItem() {
        this.setRootItem(new TreeItem<>());
        
    }

    public String getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(String currentFile) {
        this.currentFile = currentFile;
    }

    public TreeItem<String> getRootItem() {
        return rootItem;
    }

    private void setRootItem(TreeItem<String> rootItem) {
        this.rootItem = rootItem;
    }

    public TreeView<String> getTree() {
        return tree;
    }

    public void setTree(TreeView<String> tree) {
        this.tree = tree;
    }

    public final void setTimeLine(ControlImages ctrol) {
        
        
        rootItem = new TreeItem<>("Images Tree Directory");
        totales = new TreeItem<>("Images");
        favoritos = new TreeItem<>("Favoritos");
        rootItem.getChildren().addAll(totales,favoritos);
        rootItem.setExpanded(true);
        ctrol.getImagenes().forEach((Imagenes img) -> {
            String name = new File(img.getUrlImagen()).getName();
            TreeItem<String> item = new TreeItem<>(name);
            totales.getChildren().add(item);
        });
        
       

        tree = new TreeView<>(rootItem);
        tree.setPadding(new Insets(10,10,10,10));
        tree.applyCss();

    }
    
    public void UpdateTreeView(ControlImages ctrl){
        this.tree.setEditable(true);
        this.rootItem.getChildren().add(
                new TreeItem<>(convertToName(ctrl.getImagenes().get(ctrl.getImagenes().size()-1).getUrlImagen())));
        tree = new TreeView(this.rootItem);
    }
    
    public void UpdateTreeTotally(ControlImages ctrl){
        this.tree.setEditable(true);
        rootItem.getChildren().clear();
        ctrl.getImagenes().forEach((Imagenes img) -> {
            String name = new File(img.getUrlImagen()).getName();
            System.out.println(name);
            TreeItem<String> item = new TreeItem<>(name);
            rootItem.getChildren().add(item);
        });
        tree = new TreeView(rootItem);
    }
    
    public String convertToName(String path){
        String name = new File(path).getName();
        return name;
    }
    
    public void setUpActions(){
        
        tree.setOnMouseClicked(e->{
          tree.getSelectionModel()
        .selectedItemProperty()
        .addListener((observable, oldValue, newValue) -> {
              
              currentFile = newValue.getValue();
              System.out.println(currentFile);
          });     
               
        });
    }
    
    private void setContextMenu(TreeItem<String> item, MouseEvent e){
        TextField textField = new TextField("Option"); 
        ContextMenu contextMenu = new ContextMenu();
        MenuItem eliminar = new MenuItem("Eliminar");
        MenuItem calificar = new MenuItem("Calificar");
        contextMenu.getItems().addAll(eliminar, calificar);
        
        
            textField.setContextMenu(contextMenu);
            contextMenu.show(item.getGraphic(), e.getScreenX(), e.getScreenY());
        
            eliminar.setOnAction((ActionEvent event) -> {
            
                System.out.println("Cut...");
            });
        
        if(e.isPrimaryButtonDown())
            contextMenu.setHideOnEscape(true);
            
    }
}
/* LEFT TO ADD OPTIONS TO DIRECT ADDING FILES INTO TREEITEM ON CLICK ONLY EVENTS
class MyTreeCell extends TextFieldTreeCell<String> {
    private TextField textField;
    private ContextMenu addMenu = new ContextMenu();
 
        public MyTreeCell() {
            MenuItem addMenuItem = new MenuItem("Eliminar Imagen");
            addMenu.getItems().add(addMenuItem);
            addMenuItem.setOnAction(new EventHandler() {
                public void handle(Event t) {
                    TreeItem newEmployee = 
                        new TreeItem<>("New Employee");
                            getTreeItem().getChildren().add(newEmployee);
                }

               
            });
        }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        // if the item is not empty and is a root...
        if (!empty && getTreeItem().getParent() == null) {
            
        }
    }
}*/
