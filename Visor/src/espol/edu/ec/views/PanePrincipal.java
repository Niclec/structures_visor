/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.views;

import espol.edu.ec.control.Imagenes;
import espol.edu.ec.control.VistaImages;
import java.io.File;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author user
 */
public class PanePrincipal {

    private BorderPane root, imageRoot;
    private Button next, back, upload, delete, rate;
    private Label lbl_rate;
    private ComboBox calificacion;
    private VBox tempLine;
    private TreeItem<String> rootItem;
    private VistaImages images;
    private ViewTreeItem tree;

    public PanePrincipal() {
        root = new BorderPane();
        this.root.setPadding(new Insets(10, 10, 10, 10));
        setButtoms();
        setUpImages();
        this.setUpTree();
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }

    private void setUpImages() {
        this.imageRoot = new BorderPane();
        images = new VistaImages(this.imageRoot);
        images.getRoot().autosize();
        this.root.setCenter(images.getRoot());
    }

    private void setButtoms() {

        HBox conten_superior = new HBox();
        conten_superior.setStyle("-fx-background-color: #426D96;\n" +
        "    -fx-padding: 10;\n" +"    -fx-spacing: 10;");
        conten_superior.setAlignment(Pos.BASELINE_RIGHT);
        conten_superior.setPadding(new Insets(10, 10, 10, 10));
        this.back = new Button("Back");
        this.next = new Button("Next");
        this.upload = new Button("Upload");
        this.delete = new Button("Delete");
        this.rate = new Button("Rate");
        conten_superior.getChildren().addAll(delete);
        this.setUpRating();
        this.root.setTop(conten_superior);
        this.setUpActions();
    }

    public final void setTimeLine() {
        ImageView iv = new ImageView(new Image(new File("imageFolder1.jpg").toURI().toString()));
        Node rootIcon = iv;
        rootItem = new TreeItem<>("Inbox", rootIcon);
        rootItem.setExpanded(true);
        for (int i = 1; i < 6; i++) {
            TreeItem<String> item = new TreeItem<>("Message" + i);
            rootItem.getChildren().add(item);
        }
        this.tempLine = new VBox();
        TreeView<String> tree = new TreeView<>(rootItem);
        this.tempLine.getChildren().addAll(tree);
        this.root.setLeft(this.tempLine);
    }

    private void setUpRating() {
        VBox orden = new VBox();
        HBox rating = new HBox();
        rating.setStyle("-fx-background-color: #426D96;\n" +
        "    -fx-padding: 10;" );
        this.setUpComboBox();
        this.lbl_rate = new Label("Rate this image");
        rating.setAlignment(Pos.CENTER);
        rating.setPadding(new Insets(10, 10, 10, 10));
        rating.setSpacing(30);
        rating.getChildren().addAll(this.lbl_rate, this.calificacion, this.rate);
        HBox contenedor = new HBox();
        contenedor.setStyle("-fx-background-color: #426D96;\n" +
        "    -fx-padding: 10;");
        contenedor.setAlignment(Pos.CENTER);
        contenedor.setPadding(new Insets(10, 10, 10, 10));
        contenedor.getChildren().addAll(this.back, this.next, this.upload);
        orden.getChildren().addAll(rating, contenedor);
        orden.setAlignment(Pos.CENTER);
        this.root.setBottom(orden);
    }

    private void setUpComboBox() {
        this.calificacion = new ComboBox();
        this.calificacion.setMaxWidth(100);
        this.calificacion = new ComboBox<>();
        this.calificacion.setItems(FXCollections.observableArrayList(1, 2, 3, 4, 5));
        this.calificacion.getSelectionModel().selectFirst();
    }

    private void setUpTree() {
        this.tree = new ViewTreeItem();
        tree.setTimeLine(this.images.getControl());
        this.root.setLeft(tree.getTree());
        this.treeActions();
    }

    public final void setUpActions() {
        this.upload.setOnAction(e -> {
            this.images.tomarImagen();
            this.tree.UpdateTreeTotally(this.images.getControl());
        });

        this.delete.setOnAction(e -> {
            this.images.goBack();

            this.tree.UpdateTreeTotally(this.images.getControl());
        });

        this.next.setOnAction(e -> {
            this.setInitCombo();
            this.images.changeImage(1);
        });

        this.back.setOnAction(e -> {
            this.setInitCombo();
            this.images.changeImage(0);
        });
        
        this.rate.setOnAction(e->{
            int valor = (Integer)(this.calificacion.getSelectionModel().getSelectedItem());
            this.images.getControl().getActual().setStars(valor);
        });

    }

    private void treeActions() {
        this.setInitCombo();
        this.tree.getTree().setOnMouseClicked(e -> {
            tree.getTree().getSelectionModel()
        .selectedItemProperty()
        .addListener((observable, oldValue, newValue) -> {
              
            this.tree.setCurrentFile(newValue.getValue());
            String path = this.tree.getCurrentFile();
            
            Imagenes im = this.images.getControl().getImagenesbyName(path);
            if (im.getIm() != null) {
                this.images.getControl().setActual(this.images.
                        getControl().getImagenes().get(this.images.getControl().getImagenes().indexOf(im)));
                this.images.changeImage(im);
            }
              
          });               
        });

    }
    
    private void setInitCombo(){
        this.calificacion.getSelectionModel().selectFirst();
    }
    

}
